import './about.scss';
import template from './about.html';

export class VdAbout {
    get template() {
        return template;
    }
    
    toggleAnimations() {
        const meters = document.querySelectorAll('.skill__meter-progress');
        const progress = [90, 90, 80, 70, 60, 50];
        requestAnimationFrame(() => {
            meters.forEach((meter, i) => {
                meter.classList.add(`progress-${progress[i]}`);
            })
        })
    }
}