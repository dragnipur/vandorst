import './projects.scss';
import template from './projects.html';

export class VdProjects {
    get template() {
        return template;
    }

    toggleAnimations() {
        const projects = document.querySelectorAll('.project');
        let timeout = 0;

        requestAnimationFrame(() => {
            projects.forEach((project) => {
                setTimeout(() => project.classList.add('animate'), timeout);
                timeout += 200;
            });
        });
    }
}