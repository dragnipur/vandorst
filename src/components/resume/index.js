import './resume.scss';
import template from './resume.html';

export class VdResume {
    constructor(rivets, data) {
        this.rivets = rivets;
        this.data = data;
    }

    get template() {
        return template;
    }

    bindData() {
        this.rivets.bind(document.getElementById('courses'), this.data);
    }

    toggleAnimations() {
        requestAnimationFrame(() => {
            document.querySelector('.education-content').classList.add('animate');
            document.querySelector('.timeline').classList.add('animate');
            setTimeout(() => {
                document.querySelector('.timeline-legend').classList.add('animate');
                document.querySelector('.timeline-fields').classList.add('animate');
            }, 1500);
        });
    }
}