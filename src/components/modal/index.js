import './modal.scss';
import template from './modal.html';

export class VdModal {
    constructor(rivets, data) {
        this.rivets = rivets;
        this.data = data;
    }

    get template() {
        return template;
    }

    bindData() {
        this.rivets.bind(document.getElementById('dialog-container'), this.data);
    }
}