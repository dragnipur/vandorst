import '../style/style.scss';
import rivets from '../node_modules/rivets/dist/rivets';

import { VdLanding } from './components/landing';
import { VdAbout } from './components/about';
import { VdResume } from './components/resume';
import { VdProjects } from './components/projects';
import { VdBlog } from './components/blog';
import { VdModal } from './components/modal/index';
import * as showdown from 'showdown';
import data from './data';

class cvApp {
  constructor() {
    window.onscroll = this.onscroll.bind(this);
    document.body.removeAttribute('hidden');
    this.configureCustomDatabindings();
    this.markdownConverter = new showdown.Converter();

    this.setupViews();
    this.renderData();
    this.onscroll();
  }

  configureCustomDatabindings() {
    rivets.binders.src = (el, value) => el.src = value;
    rivets.binders.hidden = (el, value) => el.hidden = value;
  }

  setupViews() {
    this.views = [
      { id: 'landing', instance: new VdLanding() },
      { id: 'about', instance: new VdAbout() },
      { id: 'resume', instance: new VdResume(rivets, data) },
      { id: 'projects', instance: new VdProjects() },
      { id: 'blog', instance: new VdBlog() },
      { id: 'modal', instance: new VdModal(rivets, data) },
    ]
  }

  renderData() {
    data.templates = this.views.reduce((accumulator, view) => {
      accumulator[view.id] = view.instance.template;
      return accumulator;
    }, {});
    rivets.bind(document.body, data);

    this.views.forEach((view) => { if (view.instance.bindData) view.instance.bindData() });
  }

  onscroll() {
    data.fabHidden = window.scrollY === 0;
    this.updateViewAnimations();
  }

  updateViewAnimations() {
    this.views.forEach((view) => {
      if (!view.shown && window.scrollY >= document.getElementById(view.id).offsetTop) {
        if (view.instance.toggleAnimations) view.instance.toggleAnimations();
        view.shown = true;
      }
    });
  }

  scrollTo(targetId) {
    const targetElement = document.querySelector(`#${targetId} span.scroll-anchor`);
    targetElement.scrollIntoView({ behavior: 'smooth' });
  }

  toggleModal(id) {
    if (id) data.dialog = this._getDialogContent(id);
    data.dialogOpen = !data.dialogOpen;
  }

  _getDialogContent(id) {
    const dialogData = data.dialogContent[id];
    if (!dialogData.htmlContent) {
      dialogData.htmlContent = this.markdownConverter.makeHtml(dialogData.content);
    }
    return dialogData;
  }
}

if (['interactive', 'complete'].includes(document.readyState)) {
  window.cvApp = new cvApp();
} else {
  document.addEventListener('DOMContentLoaded', function () {
    window.cvApp = new cvApp();
  });
}
