import img_angular from '../assets/skills/angular.svg';
import img_spring from '../assets/skills/spring-boot.png';
import img_react from '../assets/skills/react.svg';
import img_polymer from '../assets/skills/polymer.png';
import img_node from '../assets/skills/nodejs.png';
import img_vue from '../assets/skills/vuejs.png';
import img_cassandra from '../assets/skills/cassandra.svg';

export default {
    fabHidden: true,
    dialogOpen: false,

    dialog: {
        title: '',
        description: '',
        skills: []
    },

    templates: {},

    courses: [
        { title: 'Polymer', year: '2017' },
        { title: 'Secure Programming', year: '2016' },
        { title: 'AngularJS', year: '2016' },
        { title: 'HTML5 / CSS3', year: '2016' },
        { title: 'Javascript - ES6', year: '2016' },
    ],

    dialogContent: {
        internship: {
            title: 'The retrospective app',
            content: `During my internship at Info Support I worked on an app which allows teams to perform retrospectives digitally.<br/><br/>I added a mechanism which analyzes a teams retrospectives, find the most used subjects and displays them in a wordcloud. Allowing teams to easily identify returing subjects.\nThe app was build using AngularJS and Spring Boot running using Docker.`,
            skills: [img_angular, img_spring],
        },
        loans: {
            title: 'Consumer Loans',
            content: `During this project I build and managed multiple applications related to consumer loans within ING. these include the portal ING customers use to manage their loans and several support applications for ING imployees.<br/><br/> All applications where build using AngularJS powered by a Spring backend. We used the ATDD methodology to deliver new features in close cooperation with our business.`,
            skills: [img_angular, img_spring],
        },
        insurance: {
            title: 'National Insurance Portal',
            content: `This project revolved around an interactive insurace portal. It's goal is to provide one place to manage all your insurance policies, independent of the company they're registered to.<br/><br/>I worked on adding a new way of authentication using the Dutch IDIN platform in both the React front-end and the Spring backend.`,
            skills: [img_react, img_spring, img_cassandra],
        },
        developerPortal: {
            title: 'API Developer Portal / Polymer teacher',
            content: `During my second ING project I worked on an innovative project to provide a portal to expose several ING APIs to the outside world. For this purpose we build a portal using PolymerJS powered by a Java backend which allows developers to find and subscribe to ING APIs.<br/><br/>I also started teaching a 5 day PolymerJS course in this period in which I teach developers all the ins and outs of Polymer and the modern web platform.`,
            skills: [img_polymer, img_spring],
        },
        yarnbag: {
            title: 'Yarnbag.nl - A Polymer webshop',
            content: 'Yarnbag.nl is a fully featured webshop I build using PolymerJS and NodeJS. It focuses on knitting and crochet in the Netherlands.<br/><br/>The app has been build with performance and usability in mind. Making extensive use of lazy loading, webworkers and HTML templates for on demand rendering and giving the customers a great experience on every device.<br/><br/><a href="https://www.yarnbag.nl/" target="_blank">Webshop</a>',
            skills: [img_polymer, img_node],
        },
        checkers: {
            title: 'VueJS & checkers',
            content: 'A game of checkers build using VueJS and Vuex. It contains a singleplayer mode with a basic UI and multiplayer mode which allows 2 players to play against each other on the same device. An online mode (using websockets) is in the works.<br/><br/><a href="https://gitlab.com/dragnipur/vue-checkers" target="_blank">source code</a> | <a href="/checkers" target="_blank">play the game</a>',
            skills: [img_vue]
        },
        wordcloud: {
            title: 'AngularJS Wordclouds',
            content: 'Tiny-angular-wordcloud is a directive written for AngularJS which allows you to add customizable wordclouds to your projects. Being just 2kb in size without any external dependencies it\'s a great fit for any AngularJS project.<br/><br/>This project game to life when I needed wordcloud functionality in my AngularJS project but could not find any library which was not to bulky.<br/><br/><a href="https://github.com/Dragnipur/tiny-angular-wordcloud" target="_blank">source code</a>',
            skills: [img_angular]
        },
        bonzai: {
            title: 'Bonzai shopping lists',
            content: 'A shopping list managers with which you can create shopping lists for multiple shops. Bonzai also allows you to view discounted articles at different shops and add them to your shopping list.<br/><br/><a href="https://gitlab.com/dragnipur/bonzai" target="_blank">source code</a> | <a href="/bonzai" target="_blank">use the app</a>',
            skills: [img_vue, img_node]
        },
    }
};