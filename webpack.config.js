const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  devtool: 'source-map',
  mode: 'development',
  resolve: {
    alias: {
      assets: path.resolve(__dirname, 'assets/'),
      components: path.resolve(__dirname, 'src/components/')
    },
  },
  output: {
    filename: 'bundle.[hash].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.js$/,
      use: [{
        loader: 'babel-loader',
      }],
    },
    {
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    },
    {
      test: /\.(png|svg|jpg|gif|ico)$/,
      use: ['file-loader'],
    },
    {
      test: /\.html$/,
      use: [{
        loader: 'html-loader',
        options: {
          minimize: true,
          removeComments: true,
          collapseWhitespace: true
        }
      }],
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Pieter van Dorst',
      template: './template.index.html'
    }),
    new FaviconsWebpackPlugin('assets/traits/creative.svg')
  ]
};